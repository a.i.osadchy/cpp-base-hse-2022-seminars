#include "functions.h"
#include <iostream>

enum class TestResult {
    Success = 0,
    Failure = 1,
};

int main() {
    bool tests_passed = true;

    if (f() != 25) {
        tests_passed = false;
        std::cerr << "Test for \"f\" failed..." << std::endl;
    }

    if ((g(24) != 25) || (g(-1) != 0)){
        tests_passed = false;
        std::cerr << "Test for \"g\" failed..." << std::endl;
    }

    if (tests_passed) {
        return static_cast<int>(TestResult::Success);
    }
    return static_cast<int>(TestResult::Failure);
}
