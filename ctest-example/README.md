# Пример работы с cctest

Для того, чтобы собрать программу, нужно загрузить проект в IDE
или выполнить из консоли следующие команды:

```
mkdir build
cd build
cmake ..
make functions
make test_functions
make test
```
