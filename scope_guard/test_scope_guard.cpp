#include <stdexcept>

#include "scope_guard.h"

void require(bool condition) {
    if (!condition) {
        throw std::logic_error{"requirement failed"};
    }
}

void test_scope_guard() {
    int x = 0;
    ScopeGuard sg1 = [&]() { x = 1; };

    require(x == 0);

    {
        ScopeGuard sg2 = [&]() { x = 2; };
        require(x == 0);
    }

    require(x == 2);
}

int main() {
    try {
        test_scope_guard();
    } catch (...) {
        return 1;
    }
    return 0;
}
